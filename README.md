# Kennel inside

Ansible administration playbook for [iench.me](https://iench.me).

## Installing prerequisites

```sh
sudo dnf install ansible
```

```sh
ansible-galaxy collection install -p ./collections -r requirements.yml
```

## Gathering host facts

```sh
ansible --ask-vault-pass -i hosts.ini -m ansible.builtin.setup all
```

## Deploy, upgrade or backup the services

```sh
ansible-playbook --ask-vault-pass -Ki hosts.ini -e "@install.json" site.yml
ansible-playbook --ask-vault-pass -Ki hosts.ini -e "@ugrade.json" site.yml
ansible-playbook --ask-vault-pass -Ki hosts.ini -e "@backup.json" site.yml
```

## Licensing discomfort

Copyright © 2021 Martin Blanchard <tchaik[@]gmx.com>

This work is free. You can redistribute it and/or modify it under the
terms of the Do What The Fuck You Want To Public License, Version 2, as
published by Sam Hocevar. See the COPYING file for more details. It
comes without any warranty, to the extent permitted by applicable law.
